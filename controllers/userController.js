const User = require('./../models/user');

const Product = require('./../models/product');

const auth = require('./../auth');

const bcrypt = require('bcrypt');

//Register a user
module.exports.registerUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result) => {
		if(result){
			return "User with specified email address already exists!"
		}else{
			const {email, password} = reqBody

			const newUser = new User({
				email: email,
				password: bcrypt.hashSync(password, 10)
		
			})

			//save the newUser object to  the database
			return newUser.save().then( (result, error) => {
				if(result){
					return true
				}else{
					return error
				}
			})
		}
	})

}

//Login a user
 module.exports.loginUser = (reqBody) => {
 	const {email, password} = reqBody

 	return User.findOne({email: email}).then( (result, error) => {
 		// console.log(result)

 		if(result == null){
 			console.log('email null');
 			return false
 		}else{

 			 let isPwCorrect = bcrypt.compareSync(password, result.password)

 			if(isPwCorrect == true){
 			//return json web token
 				//invoke the function that creates the token upon logging in
 				//requirements in creating a token:
 					//if password matches from existing pw from database
 					return {access: auth.createAccessToken(result)}
 			}else{
 				return false
 			}
 		}
 	})
 }

//Get my orders (regular user)
module.exports.getMyOrders = (id) => {
	return User.findById(id).then((result, error) => {
		if(result !== null){
			return result.orders;
		}else{
			return error
		}	
	})
}

//Checkout
module.exports.checkOut = (userId, cart) =>{
//check if user exists
	return User.findById(userId).then(user => {
		if(user === null){
			return false
		}else{
			//push the contents of the cart to user.orders
			user.orders.push({
				products: cart.products,
				totalAmount: cart.totalAmount
			})
			return user.save().then((updatedUser, error) => {
				if(error){
					return false
				}else{
					// const index = updatedUser.orders[updatedUser.orders.length-1]
					// const currentOrder = updatedUser.orders[index]
					const currentOrder = updatedUser.orders[updatedUser.orders.length-1]
					// console.log(currentOrder)
					currentOrder.products.forEach(product => {
						Product.findById(product.productId).then(foundProduct => {
							foundProduct.orders.push({orderId:currentOrder._id})
							foundProduct.save()
						})
					})
					return true;
				}
			})
		}
	})
}

// module.exports.checkout = async (userData, cart) => {
//     //verification
//     if (userData.isAdmin == false) {

//         //save newOrder from user to database
//         const newOrder = await User.findByIdAndUpdate(userData.id, { $addToSet: { orders: cart } }, { new: true })

//         //get latest order from user
//         const latestOrder = newOrder.orders[newOrder.orders.length - 1]
//         const orderId = latestOrder._id

//         let productId
//         let productDetail
//         for (let i = 0; i < cart.products.length; i++) {
//             //find Product using product ID from cart.products[0].productId
//             productId = cart.products[i].productId
//             //save latest order id from user to Product.orders Array
//             productDetail = await Product.findByIdAndUpdate(productId,
//                 { $addToSet: { orders: [{ orderId }] } }, { new: true })
//         }

//         return {
//             "productDetails": productDetail,
//             "latestOrder": latestOrder
//         }
//     } return "Admin is not allowed to checkout"
// }



//View all the orders (Admin)
module.exports.getAllOrders = async (userData) => {
    if (userData.isAdmin == true) {

        const allOrders = await User.find().select("-password -isAdmin -__v")
        let allActiveOrders = []

        for (let i = 0; i < allOrders.length; i++) {
            if (allOrders[i].orders.length >= 1) {
                allActiveOrders.push(allOrders[i])
            }
        }

        return allActiveOrders

    } return { message: "User is not allowed to get all orders" }
}