const Product = require('../models/product');

const User = require('./../models/user');

const auth = require('../auth');

const bcrypt = require ('bcrypt');

// Get All Products
module.exports.getAll = () => {
	return Product.find({}).then(result => {
		if(result !== null){
			return result;
		}else{
			return error
		}	
	})
}

// Get All Active Products
module.exports.getAllActive = () => {
    return Product.find({isActive: true}).then( (result, err) => {
        if(err){
            return false
        }else{
            return result
        }
    })
}


//Add Products (Admin Only)
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
	return newProduct.save().then((result, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

//Update product
module.exports.editProductInfo = (productId, reqBody) => {
		const {name, description, price} = reqBody
		const updatedProduct = {
			name: name,
			description: description,
			price: price
	}
	return Product.findByIdAndUpdate(productId, updatedProduct, {new:true}).then(result => {console.log(result)
		return result
	})
}


//Archive product
module.exports.archiveProduct = (productId) => {
	return Product.findByIdAndUpdate(productId, {isActive:false}).then( (result, error) => {
		if(result == null){
			return error
		}else{
			if (result) {
				return true
			} else {
				return false
			}
		}
	})
}

//Unarchive product or Activate product
module.exports.activateProduct = (productId) => {
	return Product.findByIdAndUpdate(productId, {isActive:true}).then( (result, error) => {
		if(result == null){
			return error
		}else{
			if (result) {
				return true
			}else{
				return false
			}
		}
	})
}

//Get a specific product
module.exports.getProduct = (params) => {
	return Product.findById(params).then( (result, error) => {
		if(result == null){
			return `Product not found`
		}else{
			if(result){
				return result
			}else{
				return error
			}
		}
	})
}