const express = require('express');

const router = express.Router();

const productController = require("./../controllers/productController");

const auth = require("./../auth");

// Get All Products
router.get("/all", (req, res) => {
    	productController.getAll()
    	.then(resultFromController => res.send(resultFromController));
})

// Get All Active Products
router.get("/active", (req, res) => {
    	productController.getAllActive()
    	.then(resultFromController => res.send(resultFromController));
})


//Add Products (Admin Only)
router.post('/', auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
    if(isAdmin === true){
		productController.addProduct(req.body).then(result => res.send(result));
	}else{
		res.send('You are not authorized to add a product!');
	}
	
})

//Update product
router.put('/:productId', auth.verify, (req, res) => {
	const admin = auth.decode(req.headers.authorization).isAdmin
		if(admin == false){
			res.send (false)
		}else{
			productController.editProductInfo(req.params.productId, req.body)
			.then(result => res.send(result))
	}
})


//Archive Product
//set isActive to False
router.put("/:productId/archive", auth.verify, (req, res) => { 
const isAdmin = auth.decode(req.headers.authorization).isAdmin
    if(isAdmin === false){
		res.send(false);
	}else{
	productController.archiveProduct(req.params.productId)
	.then(resultFromController => res.send(resultFromController))	
	}
})


//Unarchive Product or Activate Product
router.put('/:productId/activate', auth.verify, (req, res) => {
	const admin = auth.decode(req.headers.authorization).isAdmin
	if(admin == false){
		res.send (false)
	}else{
		productController.activateProduct(req.params.productId)
		.then(result => res.send(result))
	}
})


//Get a specific product
router.get("/:productId", (req, res) => {
	
  		productController.getProduct(req.params.productId)
  		.then(resultFromController => res.send(resultFromController))
  	
})

module.exports = router;