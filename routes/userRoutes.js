
const express = require('express');
//Create routes

const router = express.Router();

const userController = require('./../controllers/userController');

const auth = require('./../auth');

//Register a user
router.post("/register", (req, res) => {
    userController.registerUser(req.body)
    .then(resultFromController => res.send(resultFromController));
})

//Login a user
router.post("/login", (req, res) => {
    userController.loginUser(req.body)
    .then(resultFromController => res.send(resultFromController));
})

//Get my orders (regular user)
router.get('/myOrders', auth.verify, (req, res) => {
	const {isAdmin, id} = auth.decode(req.headers.authorization);
		if(isAdmin == true){
			return res.send(false);
		}else{
			userController.getMyOrders(id).then(resultFromController => res.send(resultFromController));
	}
})

//Checkout
router.post("/checkout", auth.verify, (req, res) => {
//if admin is true
	//res.send(false)
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	 let userId = auth.decode(req.headers.authorization).id

	if(isAdmin === false){
    
		userController.checkOut(userId, req.body).then(resultFromController => res.send(resultFromController));

    }else{

    	return res.send(false)

    }

})

// //Checkout
// router.post("/checkout", auth.verify, (req, res) => {
//     let userData = auth.decode(req.headers.authorization)

//     userController.checkout(userData, req.body)
//         .then(result => {
//             res.send(result.latestOrder)
//         });
// })

//View all the orders (Admin)
router.get("/orders", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization)

    userController.getAllOrders(userData)
        .then(result => {
            res.send(result)
        });
})

module.exports = router;