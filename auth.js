const jwt = require('jsonwebtoken');
const secret = "don'tTellAnyone"


module.exports.createAccessToken = (user) => {
	//user = object because it came from the matching document of the user from the database
	
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// jwt.sign(payload, secret, {});

	return jwt.sign(data, secret, {});
}

//decode token
module.exports.decode = (token) => {
	// console.log(token) //token
	//jwt.decode(token, {})

	let slicedToken = token.slice(7, token.length);
	console.log(slicedToken)

	return jwt.decode(slicedToken, {complete: true}).payload

}

//verify
module.exports.verify = (req, res, next) => {

	//get the token(headers)
	let token = req.headers.authorization
	

	// console.log(token)

	//jwt.verify(token, secret, function)
	if(typeof token !== "undefined"){

		let slicedToken = token.slice(7, token.length);


		return jwt.verify(slicedToken, secret, (err, data) => {
			if(err){
				res.send( {auth: "failed"})
			}else{
				next()
			}
		})
	}else{
		res.send(false)
	}
}

// //isAdmin

// module.exports.isAdmin = async (req, res, next) => {
//     let token = req.header("auth-token");
//     if (!token) return res.status(401).send("No User");
//     try {
//         const verified = verify(token, process.env.SECRET);
//         req.user = verified;
//         let user = await userModel.findOne({ isAdmin });
//         if (req.user && user.isAdmin === true) {
//             return next();
//         }
//     } catch (err) {
//         return res.status(401).send({ msg: "Admin token is not valid" });
//     }
// };

// //isAdmin

