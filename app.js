
//use directive keyword require to make a module useful in the current file/module
const express = require('express');

//express function is our server stored in a constant variable app
const app = express();

//require mongoose module to be used in our entry point file index.js
const mongoose = require('mongoose');
const cors = require('cors');

let PORT = 4000;

//MIDDLEWARES
//express.json is an express framework to parse incoming json payloads
app.use(express.json()) 
app.use (express.urlencoded({extended:true}));
app.use(cors());

//Connecting userRoutes module to index.js entry point
const userRoutes = require('./routes/userRoutes');

//Product Routes
const productRoutes = require('./routes/productRoutes');


//connect to mongoDB database
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.upmgf.mongodb.net/csp2?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true}
);

//Notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Connected to Database'));


//middleware entry point url (root url before any endpoints)
app.use("/products", productRoutes);
app.use("/users", userRoutes);


//server listening to port 3001
app.listen(process.env.PORT || 4000, () => console.log(`Server is running at ${PORT}`));
